﻿module AlfaBureau.Bi.Api.Common

open System

let connectionStringAppSettingsKey = "Bi"

let toNullable (xOpt : 'a option) : Nullable<'a> =
  match xOpt with
  | Some a -> Nullable a
  | None -> Nullable null
