module AlfaBureau.Bi.Api.Repositories

open AlfaBureau.Bi.Api.Models
open Microsoft.Extensions.Configuration
open Npgsql.FSharp
open AlfaBureau.Bi.Api.Common

type IBiRepository =
  abstract member GetAllAnnualReports : unit -> AnnualReport list
  abstract member GetAllAnnualQuotationReport : unit -> AnnualQuotationReport list
  abstract member GetAllSalesPeopleReport : unit -> SalesPeopleReport list
  abstract member GetAllDistributionAreaReport : unit -> DistributionAreaReport list


type BiRepository(config : IConfiguration) =
  let db =
    config.GetConnectionString(connectionStringAppSettingsKey)
    |> Sql.connect

  interface IBiRepository with
    member _.GetAllAnnualReports() =
      db
      |> Sql.query @"SELECT
      annee,
      CAST(chiffre_affaire AS double precision) / 100 AS chiffre_affaire,
      CAST(objectif_chiffre_affaire AS double precision) / 100 AS objectif_chiffre_affaire,
      CAST(montant_devis AS double precision) / 100 AS montant_devis,
      nombre_commandes,
      CAST(prix_vente_median AS double precision) / 100 AS prix_vente_median,
      CAST(prix_vente_moyen AS double precision) / 100 AS prix_vente_moyen
      FROM public.fait_bilanannuel
      WHERE annee >= 2015;"
      |> Sql.execute (fun read ->
        {
          Year = read.intOrNone "annee"
          Turnover = read.doubleOrNone "chiffre_affaire"
          TurnoverGoal = read.doubleOrNone "objectif_chiffre_affaire"
          QuotationTotal = read.doubleOrNone "montant_devis"
          OrderAmount = read.intOrNone "nombre_commandes"
          MedianSellingPrice = read.doubleOrNone "prix_vente_median"
          AverageSellingPrice = read.doubleOrNone "prix_vente_moyen"
        })

    member _.GetAllAnnualQuotationReport() =
      db
      |> Sql.query @"SELECT
      annee,
      vendeur,
      statut_devis,
      nombre_devis
      FROM public.fait_devisannuel
      WHERE annee >= 2015
        AND vendeur != 'Divers'
        AND vendeur != 'Service';"
      |> Sql.execute (fun read ->
        {
          Year = read.intOrNone "annee"
          SalesPerson = read.stringOrNone "vendeur"
          QuotationState = read.stringOrNone "statut_devis"
          Percentage = read.doubleOrNone "nombre_devis"
        })

    member _.GetAllSalesPeopleReport() =
      db
      |> Sql.query @"SELECT
      annee,
      CAST(montant_ventes AS double precision) / 100 AS montant_ventes,
      CAST(objectif_montant_ventes AS double precision) / 100 AS objectif_montant_ventes,
      nombre_ventes,
      CAST(montant_devis AS double precision) / 100 AS montant_devis,
      nombre_devis,
      vendeur
      FROM public.fait_ventescommerciaux
      WHERE annee >= 2015"
      |> Sql.execute (fun read ->
        {
          Year = read.intOrNone "annee"
          SalesPerson = read.stringOrNone "vendeur"
          SalesTotal = read.doubleOrNone "montant_ventes"
          SalesTotalGoal = read.doubleOrNone "objectif_montant_ventes"
          SalesAmount = read.intOrNone "nombre_ventes"
          QuoteTotal = read.doubleOrNone "montant_devis"
          QuoteAmount = read.intOrNone "nombre_devis"
        })

    member _.GetAllDistributionAreaReport() =
      db
      |> Sql.query @"SELECT
      annee,
      vendeur,
      pays_client,
      CAST(chiffre_affaire AS double precision) / 100 AS chiffre_affaire
      FROM public.fait_zonedistribution
      WHERE annee >= 2015
        AND vendeur != 'Divers'
        AND vendeur != 'Service';"
      |> Sql.execute (fun read ->
        {
          Year = read.intOrNone "annee"
          SalesPerson = read.stringOrNone "vendeur"
          Country = read.stringOrNone "pays_client"
          SalesTotal = read.doubleOrNone "chiffre_affaire"
        })
