module AlfaBureau.Bi.Api.Models

type AnnualReport = {
  Year : int option
  Turnover : double option
  TurnoverGoal : double option
  QuotationTotal : double option
  OrderAmount : int option
  MedianSellingPrice : double option
  AverageSellingPrice : double option
}

type AnnualQuotationReport = {
  Year : int option
  SalesPerson : string option
  QuotationState : string option
  Percentage : double option
}

type SalesPeopleReport = {
  Year : int option
  SalesPerson : string option
  SalesTotal : double option
  SalesTotalGoal : double option
  SalesAmount : int option
  QuoteTotal : double option
  QuoteAmount : int option
}

type DistributionAreaReport = {
  Year : int option
  SalesPerson : string option
  Country : string option
  SalesTotal : double option
}
