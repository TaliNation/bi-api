﻿module AlfaBureau.Bi.Api.Controllers

open System
open AlfaBureau.Bi.Api.Models
open AlfaBureau.Bi.Api.Repositories
open AlfaBureau.Bi.Api.Common
open Giraffe
open Microsoft.AspNetCore.Http

let queryBiDb query =
  fun (next : HttpFunc) (ctx : HttpContext) ->
    let repository = ctx.GetService<IBiRepository>()
    let data = query repository
    json data next ctx

let getAnnualReports (repository : IBiRepository) =
  repository.GetAllAnnualReports()

let getAnnualQuotationReports (repository : IBiRepository) =
  repository.GetAllAnnualQuotationReport()

let getSalesPeopleReports (repository : IBiRepository) =
  repository.GetAllSalesPeopleReport()

let getDistributionAreaReports (repository : IBiRepository) =
  repository.GetAllDistributionAreaReport()
